<?php
/*
    ./app/vues/categories/show.php
    Variables disponibles
      - $categorie ARRAY(id, titre, slug)
      - $posts ARRAY(ARRAY(id, titre, slug, media, texte, datePublication, auteur))
 */
?>
<h1 class="page-header">
    Posts de la catégorie
    <small><?php echo $categorie['titre']; ?></small>
</h1>
<?php include '../app/vues/posts/liste.php'; ?>
