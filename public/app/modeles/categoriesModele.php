<?php
/*
    ./app/modeles/categoriesModele.php
    Modèle des catégories
 */

namespace App\Modeles\Categorie;

  function findAll(\PDO $connexion, array $params = []){
    $params_default = [
      'orderBy'   => 'id',
      'orderSens' => 'ASC',
      'limit'     => null
    ];
    $params = array_merge($params_default, $params);

    $orderBy   = htmlentities($params['orderBy']);
    $orderSens = htmlentities($params['orderSens']);

    $sql = "SELECT *
            FROM categories
            ORDER BY $orderBy $orderSens ";
    $sql .=  ($params['limit'] !== null)?" LIMIT :limit ;":';';

    $rs = $connexion->prepare($sql);
    if ($params['limit'] !== null):
      $rs->bindvalue(':limit', $params['limit'], \PDO::PARAM_INT);
    endif;
    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
  }

  function findOneById(\PDO $connexion, int $id){
    $sql = "SELECT *
            FROM categories
            WHERE id = :id;";
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_STR);
    $rs->execute();
    return $rs->fetch(\PDO::FETCH_ASSOC);
  }
