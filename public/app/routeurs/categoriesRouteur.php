<?php
/*
    ./app/routeurs/categoriesRouteur.php
 */
use \App\Controleurs\Categories;
include_once '../app/controleurs/categoriesControleur.php';

 switch ($_GET['categories']):
   case 'show':
     // DETAIL D'UNE CATEGORIE
     // PATTERN: /index.php?categories=show&id=x
     // CTRL: categoriesControleur
     // ACTION: show
     Categories\showAction($connexion, $_GET['id']);
    break;
 endswitch;
