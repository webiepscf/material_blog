<?php
/*
    ./app/controleurs/categoriesControleur.php
    Contrôleur des catégories
 */

namespace App\Controleurs\Categories;
use \App\Modeles\Categorie;

  function indexAction(\PDO $connexion, array $params = []){
      // Je demande la liste des categories au modèle
        include_once '../app/modeles/categoriesModele.php';
        $categories = Categorie\findAll($connexion, $params);

      // Je charge directement la vue index
        include '../app/vues/categories/index.php';
  }

  function showAction(\PDO $connexion, int $id){
      // Je demande le categorie au modèle
        include_once '../app/modeles/categoriesModele.php';
        $categorie = Categorie\findOneById($connexion, $id);

        include_once '../app/modeles/postsModele.php';
        $posts = \App\Modeles\Post\findAll($connexion, [
          'categorie' => $id,
          'orderBy'   => 'datePublication',
          'orderSens' => 'DESC'
        ]);

      // Je charge la vue show dans $content1
        GLOBAL $content1, $title;
        $title = $categorie['titre'];
        ob_start();
          include '../app/vues/categories/show.php';
        $content1 = ob_get_clean();
  }
