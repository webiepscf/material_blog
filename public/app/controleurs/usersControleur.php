<?php
/*
    ./app/controleurs/usersControleur.php
    Contrôleur des userts
 */

namespace App\Controleurs\Users;
use \App\Modeles\User;

  function loginFormAction(\PDO $connexion){
      // Je charge la vue loginForm dans $content1
        GLOBAL $content1, $title;
        $title = TITRE_USERS_LOGINFORM;
        ob_start();
          include '../app/vues/users/loginForm.php';
        $content1 = ob_get_clean();
  }

  function loginAction(\PDO $connexion, array $data = null){
    // Je demande le user qui correspond au login/pwd
    include '../app/modeles/usersModele.php';
    $user = User\findOneByLoginPwd($connexion, $data);

    // Je redirige vers le backoffice si c'est OK
    // Et vers le formulaire de connexion sinon
    if ($user):
      $_SESSION['user'] = $user;
      header('location: ' . ROOT_ADMIN);
    else:
      header('location: ' . ROOT . 'users/login/form');
    endif;
  }
