<?php
/*
    ./noyau/fonctions.php
    Fonctions personnalisées du framework
 */
namespace Noyau\Fonctions;

  function slugify(string $str) {
	    return trim(preg_replace('/[^a-z0-9]+/', '-', strtolower($str)), '-');
	}

  function datify(string $date, string $format){
    return date_format(date_create($date), $format);
  }
