<?php
/*
    ./app/vues/posts/search.php
    Variables disponibles
      - $search STRING
      - $posts ARRAY(ARRAY(id, titre, slug, datePublication, texte, media, auteur))
 */
?>
<h1 class="page-header">
    Résultats de la recherche:
    <small><?php echo $search; ?></small>
</h1>
<?php include '../app/vues/posts/liste.php'; ?>
