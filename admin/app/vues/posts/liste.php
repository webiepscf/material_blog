<?php
/*
    ./app/vues/posts/liste.php
    Variables disponibles
      - $posts ARRAY(ARRAY(id, titre, slug, datePublication, texte, media, auteur))
 */
?>
<?php foreach ($posts as $post): ?>
  <h2>
      <a href="posts/<?php echo $post['idPost']; ?>/<?php echo $post['slug']; ?>">
        <?php echo $post['titrePost']; ?>
      </a>
  </h2>
  <p class="lead">
    by <a href="#"><?php echo $post['pseudo']; ?></a>
  </p>
  <p> Posted on
    <?php
      //$date = new DateTime($post['datePublication']);
      //echo $date->format('D M Y');
     ?>
    <?php echo Noyau\Fonctions\datify($post['datePublication'],"D M Y"); ?>     </p>
  <hr>
  <img class="img-responsive z-depth-2" src="<?php echo $post['media']; ?>" alt="<?php echo $post['titrePost']; ?>">
  <hr>
     <div><?php echo $post['texte']; ?></div>
  <a href="posts/<?php echo $post['idPost']; ?>/<?php echo $post['slugPost']; ?>">
    <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
  </a>
  <hr>
<?php endforeach; ?>
