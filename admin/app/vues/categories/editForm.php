<?php
/*
    ./app/vues/categories/show.php
    Variables disponibles
      - $categorie ARRAY(id, titre, slug)
 */
?>
<h1>Modification d'un enregitrement</h1>
<div>
  <a href="<?php echo ROOT; ?>categories">
    Retour vers la liste des enregistrements
  </a>
</div>

<form action="<?php echo ROOT; ?>categories/edit/<?php echo $categorie['id']; ?>" method="post" class="edit">
  <div>
    <label for="titre">Titre</label>
    <input type="text" name="titre" id="titre" value="<?php echo $categorie['titre']; ?>" />
  </div>
  <div><input type="submit" /></div>
</form>
