<?php
/*
    ./app/routeurs/postsRouteur.php
 */
use \App\Controleurs\Posts;
include_once '../app/controleurs/postsControleur.php';

 switch ($_GET['posts']):
   case 'show':
     // DETAIL D'UN POST
     // PATTERN: /index.php?posts=show&id=x
     // CTRL: postsControleur
     // ACTION: show
       Posts\showAction($connexion, $_GET['id']);
       break;

    case 'search';
      // RECHERCHE D'UN POST
      // PATTERN: /index.php?posts=search
      // CTRL: postsControleur
      // ACTION: search
      Posts\searchAction($connexion, $_POST['search']);
      break;
 endswitch;
