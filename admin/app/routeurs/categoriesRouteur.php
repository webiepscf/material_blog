<?php
/*
    ./app/routeurs/categoriesRouteur.php
 */
use \App\Controleurs\Categories;
include_once '../app/controleurs/categoriesControleur.php';

 switch ($_GET['categories']):
   case 'index':
     // LISTE DES CATEGORIES
     // PATTERN: /index.php?categories=index
     // CTRL: categoriesControleur
     // ACTION: index
     Categories\indexAction($connexion);
    break;
    case 'addForm':
      // AJOUT CATEGORIE: FORMULAIRE
      // PATTERN: /index.php?categories=addForm
      // CTRL: categoriesControleur
      // ACTION: addForm
      Categories\addFormAction();
     break;
     case 'add':
       // AJOUT CATEGORIE: INSERT
       // PATTERN: /index.php?categories=add
       // CTRL: categoriesControleur
       // ACTION: add
       Categories\addAction($connexion, [
         'titre' => $_POST['titre']
       ]);
      break;
      case 'delete':
        // SUPPRESSION CATEGORIE
        // PATTERN: /index.php?categories=delete&id=x
        // CTRL: categoriesControleur
        // ACTION: delete
        Categories\deleteAction($connexion, $_GET['id']);
       break;
       case 'editForm':
         // MODIFIOCATION D'UN CATEGORIE: FORMULAIRE
         // PATTERN: /index.php?categories=editForm&id=x
         // CTRL: categoriesControleur
         // ACTION: editForm
         Categories\editFormAction($connexion, $_GET['id']);
        break;
        case 'edit':
          // MODIFIOCATION D'UN CATEGORIE: UPDATE
          // PATTERN: /index.php?categories=edit&id=x
          // CTRL: categoriesControleur
          // ACTION: edit
          Categories\editAction($connexion, [
            'id'    => $_GET['id'],
            'titre' => $_POST['titre']
          ]);
         break;
 endswitch;
