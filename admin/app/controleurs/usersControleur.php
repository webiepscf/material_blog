<?php
/*
    ./app/controleurs/usersControleur.php
    Contrôleur des userts
 */

namespace App\Controleurs\Users;
use \App\Modeles\User;

  function dashboardAction(\PDO $connexion){
      // Je charge la vue dashboard dans $content1
        GLOBAL $content1, $title;
        $title = TITRE_USERS_DASHBOARD;
        ob_start();
          include '../app/vues/users/dashboard.php';
        $content1 = ob_get_clean();
  }

  function logoutAction(){
    // Je tue la variable de session 'user'
      unset($_SESSION['user']);
    // Je redirige vers le site public
      header('location: ' . ROOT_PUBLIC);
  }
