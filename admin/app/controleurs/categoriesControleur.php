<?php
/*
    ./app/controleurs/categoriesControleur.php
    Contrôleur des catégories
 */

namespace App\Controleurs\Categories;
use \App\Modeles\Categorie;

  function indexAction(\PDO $connexion, array $params = []){
      // Je demande la liste des categories au modèle
        include_once '../app/modeles/categoriesModele.php';
        $categories = Categorie\findAll($connexion, $params);

      // Je charge la vue index dans $content1
      GLOBAL $content1, $title;
      $title = TITRE_CATEGORIES_INDEX;
      ob_start();
        include '../app/vues/categories/index.php';
      $content1 = ob_get_clean();
  }

  function addFormAction(){
    // Je charge la vue index dans $content1
    GLOBAL $content1, $title;
    $title = TITRE_CATEGORIES_ADDFORM;
    ob_start();
      include '../app/vues/categories/addForm.php';
    $content1 = ob_get_clean();
  }

  function addAction(\PDO $connexion, array $data = null){
    // Je demande au modèle d'ajouter la catégorie
    include_once '../app/modeles/categoriesModele.php';
    $id = Categorie\insert($connexion, $data);

    // Je redirige vers la liste des catégories
    header('location: ' . ROOT . 'categories');
  }

  function deleteAction(\PDO $connexion, int $id){
    // Je demande au modèle de supprimer la catégorie
    include_once '../app/modeles/categoriesModele.php';
    $return = Categorie\delete($connexion, $id);

    // Je redirige vers la liste des catégories
    header('location: ' . ROOT . 'categories');
  }

  function editFormAction(\PDO $connexion, int $id){
    // Je demande au modèle de supprimer la catégorie
    include_once '../app/modeles/categoriesModele.php';
    $categorie = Categorie\findOneById($connexion, $id);

    // Je charge la vue editForm dans $content1
    GLOBAL $content1, $title;
    $title = TITRE_CATEGORIES_EDITFORM;
    ob_start();
      include '../app/vues/categories/editForm.php';
    $content1 = ob_get_clean();
  }

  function editAction(\PDO $connexion, array $data = null){
    // Je demande au modèle d'updater la catégorie
    include_once '../app/modeles/categoriesModele.php';
    $return = Categorie\update($connexion, $data);

    // Je redirige vers la liste des catégories
    header('location: ' . ROOT . 'categories');
  }
